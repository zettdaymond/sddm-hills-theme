# Description #

**ATTENTION!** This repo include 26.5 Mb video file. Be careful in cloning, cause it may use much of your traffic.

SDDM Hills Theme is based on two themes :

* [Maui](https://github.com/sddm/sddm/tree/master/data/themes/maui)
* [Numix](https://github.com/intialonso/intialonso.github.io)

and contains performance fix for Intel i915 video card that removes freezes and reduces unjustified CPU utilization (at least, on my ASUS Eee PC 1215P).

If QML works fine on your mashine, or SDDM works without freezes with any theme, you will not feel the difference.

SDDM Hills Theme also supports looped video background. 

![hills.jpg](https://bitbucket.org/repo/EEAdBz/images/4005479140-hills.jpg)

# Instalation. #

* Choose and download archive on [this page](https://bitbucket.org/zettdaymond/sddm-hills-theme/downloads)
* Extract archive on disc.
* Copy extracted ```hills``` folder to SDDM theme folder. By default : ```/usr/share/sddm/themes```

# Configuration #

Open ```/etc/sddm.conf``` in your favorite text editor and change follow strings :

```
[Theme]
# Current theme name
Current=hills
```

You also may change theme configuration.

Go to the theme folder and open file ```theme.conf``` in text editor.

```
[General]
background_video=resources/background/background.mp4
background_image=resources/background/background.png

#if 'true' then show background_video in loop, else show background_image.
use_video_instead_image=true
```

# Links #
* Background video : http://churchmediadesign.tv/painted-hillside-free-looping-background/
* IconSet from Numix and Maui theme. See links in **Description**